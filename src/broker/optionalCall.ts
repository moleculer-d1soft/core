import { Errors } from 'moleculer';

/**
 * Decorates broker call
 * Used to handle exceptions on service not found or service unavailable
 * 
 * @param call Call callback
 * @param types Filtered error types
 */
export async function optionalCall<T>(
  call: () => Promise<T>,
  types: string[] = ['SERVICE_NOT_FOUND', 'SERVICE_NOT_AVAILABLE']
): Promise<T | undefined> {
  try {
    return call();
  } catch (e: unknown) {
    if (!types.includes((<Errors.MoleculerError>e).type)) {
      throw e;
    }
  }
}