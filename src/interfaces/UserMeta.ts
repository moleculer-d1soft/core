export interface UserMeta {
    id: string;
    email: string | null;
    createdAt: string;
}