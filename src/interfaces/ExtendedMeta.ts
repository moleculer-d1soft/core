import { Cookie } from './Cookie';
import { UserMeta } from './UserMeta';

export interface BaseMeta {
	clientIp?: string;
	cookies?: Record<string, string | Cookie>;

	/**
	 * @url https://moleculer.services/docs/0.14/moleculer-web.html#Response-type-amp-status-code
	 */
	$statusCode?: number;
	$statusMessage?: string;
	$responseType?: string;
	$responseHeaders?: Record<string, any>;
	$location?: string;
}

export interface ExtendedMeta extends BaseMeta {
	user: UserMeta;
	token: string;
}