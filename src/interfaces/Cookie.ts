/**
 * @url https://developer.mozilla.org/ru/docs/Web/HTTP/Headers/Set-Cookie
 */

export interface Cookie {
	value: string;
	expires?: Date;
	maxAge?: number;
	domain?: string;
	path?: string;
	secure?: boolean;
	httpOnly?: boolean;
	sameSite?: 'Strict' | 'Lax' | 'None';
}
