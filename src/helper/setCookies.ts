import { Http2ServerResponse } from 'http2';
import { BaseMeta } from '../interfaces';

/**
 * Sets cookies to response as Set-Cookie headers
 * 
 * @param res Response
 * @param cookies Cookies list
 */
export function setCookies(
  res: Http2ServerResponse,
  cookies?: BaseMeta['cookies']
): void {
  if (!cookies) {
    return;
  }

  const serialized = Object.entries(cookies).map(([name, value]) => {
    if (typeof value === 'string') {
      return `${name}=${value};`;
    }

    const cookie: string[] = [`${name}=${value.value}`];
    if (value.expires) {
      cookie.push(`Expires=${value.expires.toUTCString()}`);
    }

    if (value.httpOnly) {
      cookie.push('HttpOnly');
    }

    if (value.secure) {
      cookie.push('Secure')
    }

    if (value.domain) {
      cookie.push(`Domain=${value.domain}`);
    }

    if (value.maxAge) {
      cookie.push(`MaxAge=${value.maxAge}`);
    }

    if (value.path) {
      cookie.push(`Path=${value.path}`);
    }

    if (value.sameSite) {
      cookie.push(`SameSite=${value.sameSite}`);
    }
    
    return cookie.join(';');
  });

  res.setHeader('Set-Cookie', serialized);
}