export interface Paginable {
    page: number;
    count: number;
}