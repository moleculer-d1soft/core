export interface Searchable {
    search?: string;
}