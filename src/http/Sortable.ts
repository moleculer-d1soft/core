import { SortDirection } from './SortDirection';

/**
 * Sorting trait for http request options
 */
export interface Sortable<T> {
    sort?: T;
    order?: SortDirection;
}