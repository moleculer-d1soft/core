import { Filterable } from './Filterable';

export interface DateFilter extends Filterable {
    from?: string;
    to?: string;
}