# Moleculer d1soft / Core

Base application interfaces and moleculer extendings.

- Optional broker call function
- Extended Meta (with API GW and other features)
- Cookies helpers
- CRUD-s interfaces

## Install

```sh
npm add -E @moleculer-d1soft/core
```